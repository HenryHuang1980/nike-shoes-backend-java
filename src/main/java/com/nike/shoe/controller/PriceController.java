package com.nike.shoe.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nike.shoe.Constant;
import com.nike.shoe.model.ResultVO;
import com.nike.shoe.model.ShoesPriceVO;
import com.nike.shoe.service.ShoesPriceService;

@RestController
@RequestMapping("/api")
public class PriceController {
	
	@Autowired
	private ShoesPriceService shoesPriceService;
	
    @GetMapping("/shoe-price/{id}")
    @ResponseBody
	public ResultVO obtainShoesPrice(@PathVariable String id) {
    	ResultVO result = new ResultVO();
    	BigDecimal orgPrice = shoesPriceService.randomPrice(id);
    	if(orgPrice == null) {
			result.setStatusCode(Constant.FAILED_CACULATE);
			result.setErrorMsg("Cannot obtain a random price for the shoes: " + id);
			return result;    		
    	}
    	BigDecimal discPrice = shoesPriceService.discountPrice(orgPrice);
    	if(discPrice == null) {
			result.setStatusCode(Constant.FAILED_CACULATE);
			result.setErrorMsg("Cannot discuount the price: " + discPrice);
			return result;    		
    	}
    	ShoesPriceVO priceVO = new ShoesPriceVO();
    	priceVO.setOriginalPrice(orgPrice);
    	priceVO.setDiscountedPrice(discPrice);
    	result.setData(priceVO);
    	return result;
	}
}
