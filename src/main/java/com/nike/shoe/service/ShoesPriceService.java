package com.nike.shoe.service;

import java.math.BigDecimal;

/**
 * Calculate the price for a pair of specific shoes.
 * @author Henry Huang
 *
 */
public interface ShoesPriceService {
	/**
	 * Give a random price for a pair of shoes specified by its id
	 * @param shoeId ID of the pair of shoes
	 * @return random price
	 */
	public BigDecimal randomPrice(String shoeId);
	
	/**
	 * Discount the original price of a pair of shoes
	 * @param orgPrice original price
	 * @return discounted price
	 */
	public BigDecimal discountPrice(BigDecimal orgPrice);
}
