package com.nike.shoe.service.impl;

import java.math.BigDecimal;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.nike.shoe.service.ShoesPriceService;

@Service
public class ShoesPriceServiceImpl implements ShoesPriceService {
	
	/**
	 * Generate a random float by Random, then multiply 500
	 */
	public BigDecimal randomPrice(String shoeId) {
		if(shoeId == null) {
			return null;
		}
		Random random = new Random();
		float price = random.nextFloat() * 500;
		BigDecimal result = new BigDecimal(price);
		return result.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * Discount the original price by 40%
	 */
	public BigDecimal discountPrice(BigDecimal orgPrice) {
		if(orgPrice == null) {
			return null;
		}
		BigDecimal discountRate = new BigDecimal(0.6);
		BigDecimal result = orgPrice.multiply(discountRate);
		return result.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
}
