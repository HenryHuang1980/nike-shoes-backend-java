package com.nike.shoe.model;

import java.io.Serializable;

/**
 * A general value object for returning data to a request
 * @author Henry Huang
 *
 */
public class ResultVO implements Serializable {
	private static final long serialVersionUID = 81234567890L;
	private Object data;
	private int statusCode;
	private String errorMsg;
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
