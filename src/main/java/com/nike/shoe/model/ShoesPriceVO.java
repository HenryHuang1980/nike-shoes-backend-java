package com.nike.shoe.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Price object of a pair of shoes
 * @author Henry Huang
 *
 */
public class ShoesPriceVO implements Serializable {
	private static final long serialVersionUID = 81234567890L;
	private BigDecimal originalPrice;
	private BigDecimal discountedPrice;
	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}
	public BigDecimal getDiscountedPrice() {
		return discountedPrice;
	}
	public void setDiscountedPrice(BigDecimal discountedPrice) {
		this.discountedPrice = discountedPrice;
	}	
}
