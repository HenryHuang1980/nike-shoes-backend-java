package com.nike.shoe.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PriceControllerTest {

	@Autowired
	private WebApplicationContext wac;	
	
	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	public void testConnection() throws Exception {
		mvc.perform(MockMvcRequestBuilders
				.get("/api/shoe-price/hi")
				.accept(MediaType.APPLICATION_JSON_UTF8)
		).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testObtainPrice() throws Exception {
		mvc.perform(MockMvcRequestBuilders
				.get("/api/shoe-price/123")
				.accept(MediaType.APPLICATION_JSON_UTF8)
		).andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.statusCode").value(0))
        .andExpect(MockMvcResultMatchers.jsonPath("$.data.originalPrice").isNumber())
        .andExpect(MockMvcResultMatchers.jsonPath("$.data.discountedPrice").isNumber());
	}	
}
