package com.nike.shoe.service;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShoesPriceServiceTest {
	@Autowired
	private ShoesPriceService shoesPriceService;
	
	@Test
    public void testRandomPrice() throws Exception {
        Assert.assertNotNull("Cannot obtain a random price", shoesPriceService.randomPrice("123"));
        Assert.assertNull("Should return null", shoesPriceService.randomPrice(null));
	}
	
	@Test
    public void testDiscountPrice() throws Exception {
		BigDecimal orgPrice = new BigDecimal(50.58);
        Assert.assertEquals("20.23", shoesPriceService.discountPrice(orgPrice).toPlainString());
        Assert.assertNull("Should return null", shoesPriceService.discountPrice(null));
	}	
}
